//
// Created by jmmut on 10/02/16.
//

#ifndef ENUM_ENUM_H
#define ENUM_ENUM_H

#include <initializer_list>
#include <map>
#include <stdexcept>
#include <sstream>

template<typename E>
class EnumChecker {

public:

    std::initializer_list<const char*> names_list;
//    using EnumType = E;

    constexpr EnumChecker(std::initializer_list<const char*> names_list)
            : names_list(
                    names_list.size() == int(E::LAST_MARKER)
                            ? names_list
                            : throw std::invalid_argument(
                                    "this <throw-expression> is not a constant-expression because the list of names of the Enum should have length " + std::to_string(int(E::LAST_MARKER)) + ", not " + std::to_string(names_list.size()))) { }
};


#endif //ENUM_ENUM_H
