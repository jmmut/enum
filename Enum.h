//
// Created by jmmut on 10/02/16.
//

#ifndef ENUM_ENUMNAMES_H
#define ENUM_ENUMNAMES_H

#include "EnumChecker.h"

template <typename E>
class Enum {
public:
    std::map<E, std::string> names;

    Enum(EnumChecker<E> enumChecker) {

        int i = 0;

        for (auto name : enumChecker.names_list) {
            names[E(i++)] = name;
        }
    }

    const std::string& operator[](E anEnum) {
        return names[anEnum];
    }
};


#endif //ENUM_ENUMNAMES_H
