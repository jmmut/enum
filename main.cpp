#include <iostream>
#include "Enum.h"
#include "SmartEnum.h"

using namespace std;


int main() {
    enum class Myenum {THING, FOO, BAR, LAST_MARKER};
    constexpr EnumChecker<Myenum> myenumChecker({"THING", "FOO", "BAR"});
//    constexpr EnumChecker<Myenum> myenumChecker({"THING", "FOO", "BAR", "wrong number of enum items"});// this should not compile
    Enum<Myenum> myenum(myenumChecker);

    cout << "Hello, World!" << endl;
    cout << myenum[Myenum::THING] << endl;

    SMART_ENUM(Otherenum, FOO, BAR, BAZ)
    cout << Otherenum(Otherenum::BAZ).toString() << endl;

    if (1) {
        class inner {
        public:
            void wow() {
                cout << "wow" << endl;
            }
        };
        inner in;
        in.wow();
    }

    return 0;
}
